﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace LabNumber4_Book_
{
    class Customer : Person
    {
        private int _typediscount;
        public string typediscount { get { return typediscount; } }
        private string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Customer.txt");
        private string pathtemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp.txt");
        /// <summary>
        /// Додаємо нового клієнта і записуємо його дані в файл.
        /// </summary>
        public override void AddNewPerson()
        {
            
            Console.Write("Enter the customer ID: ");
            _perscode = Convert.ToInt32(Console.ReadLine());
            if(SearchPersonOperation(_perscode))
            {
                Console.WriteLine("Such a client already exists.");
            }
            else
            {
                Console.Write("Re-enter the customer ID: ");
                _perscode = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter surname: ");
                _surname = Console.ReadLine();
                Console.Write("Enter name: ");
                _name = Console.ReadLine();
                Console.Write("Enter phone number: ");
                _phonenumber = Console.ReadLine();
                Console.Write("Enter email: ");
                _email = Console.ReadLine();
                Console.Write("Enter type of discount(10/25/50): ");
                _typediscount = Convert.ToInt32(Console.ReadLine());
                using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
                {
                    sw.WriteLine(_perscode + "|" + _surname + "|" + _name + "|" + _phonenumber + "|" + _email + "|" + _typediscount);
                    sw.Close();
                }
            }
            
        }
        /// <summary>
        /// Виводить інформацію про всіх клієнтів
        /// </summary>
        public override void PrintAll()
        {
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        PrintDataCustomer();
                    }
                    sr.Close();
                }
            }
            catch
            {
                Console.WriteLine("Error opening file!(Customer.cs PrintAll())");
            }
        }
        /// <summary>
        /// Виводить інформацію про одного клієнта
        /// </summary>
        private void PrintDataCustomer()
        {
            Console.WriteLine("Customer ID: " + _perscode);
            Console.WriteLine("Surname: " + _surname);
            Console.WriteLine("Name: " + _name);            
            Console.WriteLine("Phone number: " + _phonenumber);
            Console.WriteLine("Email: " + _email);
            Console.WriteLine("Type of discount: " + _typediscount);
            Console.WriteLine("\n");
        }
        /// <summary>
        /// Шукає клієнта серед файлу.
        /// </summary>
        /// <param name="searchedcode"></param>
        /// /// <returns>True or False</returns>
        public override bool SearchPersonOperation(int searchedcode)
        {
            bool search = false;
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        if (searchedcode == _perscode)
                        {
                            Console.WriteLine("Success! Record found. ");
                            search = true;
                            PrintDataCustomer();
                            break;
                        }
                        else
                        {
                            search = false;
                        }
                    }
                    sr.Close();

                }
            }
            catch
            {
                Console.WriteLine("Error opening file! (Customer.cs SearchPersonOperation())");
            }
            if (!search)
            {
                Console.WriteLine("Failed");
                return false;
            }
            return true;
        }
        public void Search()
        {
            Console.Write(" Enter code: ");
            int searchedcode = Convert.ToInt32(Console.ReadLine());
            SearchPersonOperation(searchedcode);
        }
        /// <summary>
        /// Видаляє запис про клієнта.
        /// </summary>
        public void Delete()
        {
            DeletePerson(path, pathtemp);
        }
        /// <summary>
        /// Змінює інформацію про клієнта
        /// </summary>
        public override void EditPerson()
        {
            Console.Write("Enter the code to change: ");
            int id = Convert.ToInt32(Console.ReadLine());
            if (SearchPersonOperation(id))
            {
                    string line = null;
                    try
                    {
                        List<string> words = new List<string>();
                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                        {
                            using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                            {
                                while ((line = sr.ReadLine()) != null)
                                {
                                    words = (line.Split('|')).ToList();
                                    Divide(words);
                                    if (id == perscode)
                                    {
                                        Console.WriteLine("What to edit ? \n 1-Surname \n 2-Name \n 3-Phone number \n 4-Email \n 5-Discount \n 0 - Cancel operation.\n");
                                        int edit_chose = Convert.ToInt32(Console.ReadLine());
                                        switch (edit_chose)
                                        {
                                            case 1:
                                                {
                                                    Console.Write("New surname: ");
                                                    _surname = Console.ReadLine();
                                                }
                                                break;
                                            case 2:
                                                {
                                                    Console.Write("New name: ");
                                                    _name = Console.ReadLine();
                                                }
                                                break;
                                            case 3:
                                                {
                                                    Console.Write("New phone number: ");
                                                    _phonenumber = Console.ReadLine();
                                                }
                                                break;
                                            case 4:
                                                {
                                                    Console.Write("New email: ");
                                                    _email = Console.ReadLine();
                                                }
                                                break;
                                            case 5:
                                                {
                                                    Console.Write("New type of discount: ");
                                                    _typediscount = Convert.ToInt32(Console.ReadLine());
                                                }
                                                break;
                                            case 0:
                                                {
                                                     Console.Write("Operation canceled. ");
                                                }
                                                break;
                                            default:
                                                {
                                                    Console.WriteLine("You entered incorrect number.");
                                                }
                                                break;
                                        }
                                        Console.WriteLine("\n");
                                        PrintDataCustomer();
                                        sw.WriteLine(_perscode + "|" + _surname + "|" + _name + "|" + _phonenumber + "|" + _email + "|" + _typediscount);
                                    }
                                    else
                                        if (id != _perscode)
                                        {
                                            sw.WriteLine(line);
                                        }   
                                }
                                sw.Close();
                            }
                            sr.Close();
                        }
                        File.Delete(path);
                        File.Move(pathtemp, path);
                    }
                    catch
                    {
                        Console.WriteLine("Error opening file!(Customer.cs EditPerson())");
                    }                
            }
            else
            {
                Console.WriteLine("Record does not exist.");
            }
        }
        /// <summary>
        /// Ділить запис в нашому файлі на слова.
        /// </summary>
        /// <param name="words"></param>
        private void Divide(List<string> words)
        {
            _perscode = Convert.ToInt32(words[0]);
            _surname = words[1];
            _name = words[2];
            _phonenumber = words[3];
            _email = words[4];
            _typediscount = Convert.ToInt32(words[5]);
        }
    }
}
