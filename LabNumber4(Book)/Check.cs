﻿using System;
namespace LabNumber4_Book_
{
    enum OrderStatus : int { New, Paid, Canceled}
    class Check : Purchase
    {
        /// <summary>
        /// Визначаємо статус замовлення та друкуємо чек.
        /// </summary>
        public void CheckOperation()
        {
            OrderStatus orderStatus;
            Order();
            orderStatus = OrderStatus.New;
            Console.WriteLine("-----------------");
            Console.WriteLine("Order Status: " + orderStatus);
            Console.WriteLine("-----------------");
            Print();
            Console.WriteLine("-----------------");
            bool status = true;
            do
            {
                Console.WriteLine("Change order status :\n 1 - Paid\n 2 - Canceled\n");
                Console.Write("Enter: ");
                int statusselectoin = Convert.ToInt32(Console.ReadLine());
                switch (statusselectoin)
                {
                    case 1:
                        {
                            orderStatus = OrderStatus.Paid;
                            Console.WriteLine("-----------------");
                            Console.WriteLine("Order Status: " + orderStatus);
                            Console.WriteLine("-----------------");
                            PrintAndDelete();
                            Console.WriteLine("-----------------");
                            status = false;
                        }break;
                    case 2:
                        {
                            orderStatus = OrderStatus.Canceled;
                            Console.WriteLine("-----------------");
                            Console.WriteLine("Order Status: " + orderStatus);
                            Console.WriteLine("-----------------");
                            PrintAndDelete();
                            Console.WriteLine("-----------------");
                            status = false;
                        }break;
                    default:
                        {
                            Console.WriteLine("You enter an incorrect number, repeat ...");
                        }
                        break;                
                }
            } while (status == true);
        }
    }
}
