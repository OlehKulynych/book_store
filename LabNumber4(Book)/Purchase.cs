﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace LabNumber4_Book_
{
    
    class Purchase
    {
        protected int _checkbookid;
        protected string _checkbookname;
        protected int _checkbookquantity;
        protected double _checkprice;
        protected double _totalprice;
        protected int _checkemployeeid;
        protected string _checkemployeesurname;
        protected string _checkemployeename;
        protected int _checkcustomerid;
        protected string _checkcustomersurname;
        protected string _checkcustomername;
        protected int _checkdiscount;

        protected string pathorder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Purchase.txt");
        protected string pathcustomer = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TempCustomer.txt");
        protected string pathemployee = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TempEmployee.txt");
        /// <summary>
        /// Метод покупки.
        /// </summary>
        protected void Order()
        {
            bool order = true ;
            Book book = new Book();
            int numberpurchase = 0;
            do
            {
                Console.WriteLine("-----------------");
                Console.WriteLine("Enter 1 if you want to add the book to the cart or another number to complete your purchase: ");
                int operation = Convert.ToInt32(Console.ReadLine());
                switch (operation)
                {
                    case 1:
                        {
                            Console.WriteLine("-----------------");
                            Console.Write("Enter the code: ");
                            int code = Convert.ToInt32(Console.ReadLine());
                            if (book.SearchedBookOperation(code))
                            {
                                Console.WriteLine("Add this book? Enter 1 to confirm. ");
                                int confirm = Convert.ToInt32(Console.ReadLine());
                                if (confirm == 1)
                                {
                                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Book.txt");
                                    try
                                    {
                                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                                        {

                                            string line;
                                            List<string> words = new List<string>();
                                            while ((line = sr.ReadLine()) != null)
                                            {
                                                words = (line.Split('|')).ToList();
                                                _checkbookid = Convert.ToInt32(words[0]);
                                                _checkbookname = words[1];                                               
                                                _checkprice = Convert.ToDouble(words[8]);
                                                if(code == _checkbookid)
                                                {
                                                    Console.WriteLine("Specify the number of books: ");
                                                    book.quantity = Convert.ToInt32(Console.ReadLine());
                                                    _checkprice *= book.quantity;
                                                    using (StreamWriter sw = new StreamWriter(pathorder, true, Encoding.Default))
                                                    {
                                                        sw.WriteLine(_checkbookid + "|" + _checkbookname + "|" + book.quantity + "|" + _checkprice);
                                                        sw.Close();
                                                    }
                                                    numberpurchase++;
                                                    break;                                                    
                                                }
                                            }
                                            sr.Close();
                                            book.EditNumberCopies(book.quantity, code);
                                            
                                        }
                                    }
                                    catch
                                    {
                                        Console.WriteLine("Error opening file! Purchase.cs Order()");
                                    }
                                }
                            }
                        }break;
                    default:
                        {
                            Console.WriteLine("-----------------");
                            Console.WriteLine("You have completed your purchase.");
                            order = false;
                            
                        }
                        break;
                }
            } while (order == true);
            Console.WriteLine("-----------------");
            CustomerOrder();
            EmployeeOrder();            
        }
        /// <summary>
        /// Рахуємо ціну.
        /// </summary>
        /// <param name="numberpurchase"></param>
        /// <param name="discount"></param>
        /// <returns>Ціну</returns>
        private double CalculatePrice(int numberpurchase,int discount)
        {
            double[] price = new double[numberpurchase];
            double sumprice = 0;
            int i = 0;
            try
            {
                using (StreamReader sr = new StreamReader(pathorder, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        _checkprice = Convert.ToDouble(words[3]);
                        price[i] = _checkprice;                        
                        i++;
                    }
                    sr.Close();
                }
                for (int j = 0; j < numberpurchase; j++)
                {
                    sumprice += price[j];
                }
                _totalprice = sumprice - ((sumprice*discount) / 100);
            }
            catch
            {
                
                Console.WriteLine("Error opening file!(Purchase.cs CalculatePrice())");
            }
            return _totalprice;
        }
        /// <summary>
        /// Виводить інформацію про заказ.
        /// </summary>
        protected void Print()
        {
            int numberpurchase = 0;
            try
            {
                Console.WriteLine("Your order: ");
                Console.WriteLine("-----------------");
                using (StreamReader sr = new StreamReader(pathorder, Encoding.Default))
                {

                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        _checkbookid = Convert.ToInt32(words[0]);
                        _checkbookname = words[1];
                        _checkbookquantity = Convert.ToInt32(words[2]);
                        _checkprice = Convert.ToDouble(words[3]);
                        Console.WriteLine("Book code: " + _checkbookid);
                        Console.WriteLine("Title: " + _checkbookname);
                        Console.WriteLine("Quantity: " + _checkbookquantity);
                        Console.WriteLine("Price: " + _checkprice);
                        Console.WriteLine("-----------------");
                        numberpurchase++;
                    }
                    sr.Close();
                }
                Console.WriteLine("Customer: " + _checkcustomersurname + " " + _checkcustomername);
                Console.WriteLine("-----------------");
                Console.WriteLine("Employee: " + _checkemployeesurname + " " + _checkemployeename);
                Console.WriteLine("Total price: " + CalculatePrice(numberpurchase, GetDiscount()));
            }
            catch
            {
                Console.WriteLine("Error opening file!(Purcase.cs Print())");
            }

        }
        /// <summary>
        /// Виводить інформацію про заказ та видаляє файли.
        /// </summary>
        protected void PrintAndDelete()
        {
            Print();
            File.Delete(pathorder);
            File.Delete(pathcustomer);
            File.Delete(pathemployee);
        }
        /// <summary>
        /// Визначає клієнта.
        /// </summary>
        private void CustomerOrder()
        {
            bool customerorder = true;
            Customer customer = new Customer();
            do
            {
              
                Console.Write("Enter customer ID: ");
                int idcustomer = Convert.ToInt32(Console.ReadLine());

                if (customer.SearchPersonOperation(idcustomer))
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Customer.txt");
                    try
                    {
                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                        {
                            string line;
                            List<string> words = new List<string>();
                            while ((line = sr.ReadLine()) != null)
                            {
                                words = (line.Split('|')).ToList();
                                _checkcustomerid = Convert.ToInt32(words[0]);
                                _checkcustomersurname = words[1];
                                _checkcustomername = words[2];
                                _checkdiscount = Convert.ToInt32(words[5]);
                                if (idcustomer == _checkcustomerid)
                                {
                                    using (StreamWriter sw = new StreamWriter(pathcustomer, true, Encoding.Default))
                                    {
                                        sw.WriteLine(_checkcustomerid + "|" + _checkcustomersurname + "|" + _checkcustomername + "|" + _checkdiscount);
                                        sw.Close();
                                    }
                                    break;
                                }
                            }
                            sr.Close();

                        }
                    }
                    catch
                    {
                        Console.WriteLine("Error opening file! ");
                    }
                    customerorder = false;
                }
                else
                {
                    Console.WriteLine("Such a client is not registered.");
                    Console.Write("Enter 1 if you want to register: ");
                    int yesorno = Convert.ToInt32(Console.ReadLine());
                    if(yesorno == 1)
                    {
                        customer.AddNewPerson();
                        customerorder = false;
                        Console.WriteLine("Again...");
                    }
                    else
                    {
                        Console.WriteLine("Again...");   
                    }
                    
                }                                                                                             
            } while (customerorder);
        }
        /// <summary>
        /// Визначає знижку.
        /// </summary>
        private int GetDiscount()
        {
            try
            {
                using (StreamReader sr = new StreamReader(pathcustomer, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        _checkdiscount = Convert.ToInt32(words[3]);
                    }
                    
                    sr.Close();
                }
            }
            catch
            {

                Console.WriteLine("Error opening file!");
            }
            return _checkdiscount;
        }
        /// <summary>
        /// Визначає працівника.
        /// </summary>
        private void EmployeeOrder()
        {
            bool employeeorder = true;
            Employee employee = new Employee();
            do
            {

                Console.Write("Enter employee ID: ");
                int idemployee = Convert.ToInt32(Console.ReadLine());

                if (employee.SearchPersonOperation(idemployee))
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Employee.txt");
                    try
                    {
                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                        {
                            string line;
                            List<string> words = new List<string>();
                            while ((line = sr.ReadLine()) != null)
                            {
                                words = (line.Split('|')).ToList();
                                _checkemployeeid = Convert.ToInt32(words[0]);
                                _checkemployeesurname = words[1];
                                _checkemployeename = words[2];
                                if (idemployee == _checkemployeeid)
                                {
                                    using (StreamWriter sw = new StreamWriter(pathemployee, true, Encoding.Default))
                                    {
                                        sw.WriteLine(_checkemployeeid + "|" + _checkemployeesurname + "|" + _checkemployeename);
                                        sw.Close();
                                    }
                                    break;
                                }
                            }
                            sr.Close();

                        }
                    }
                    catch
                    {
                        Console.WriteLine("Error opening file! ");
                    }
                    employeeorder = false;
                }
                else
                {
                    Console.WriteLine("Such a employee is not registered.");
                    Console.Write("Enter 1 if you want to register: ");
                    int yesorno = Convert.ToInt32(Console.ReadLine());
                    if (yesorno == 1)
                    {
                        employee.AddNewPerson();
                        employeeorder = false;
                        Console.WriteLine("Again...");
                    }
                    else
                    {
                        Console.WriteLine("Again...");
                    }

                }
            } while (employeeorder);
        }
    }
}
