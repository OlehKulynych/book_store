﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace LabNumber4_Book_
{
    class Employee : Person
    {
        private string _position;
        private string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Employee.txt");
        private string pathtemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp.txt");
        /// <summary>
        /// Додаємо нового працівника і записуємо його дані в файл.
        /// </summary>
        public override void AddNewPerson()
        {
            Console.Write("Enter the employee ID: ");
            _perscode = Convert.ToInt32(Console.ReadLine());
            if (SearchPersonOperation(_perscode))
            {
                Console.WriteLine("Such a employee already exists.");
            }
            else
            {
                Console.Write("Re-nter the employee ID: ");
                _perscode = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter surname: ");
                _surname = Console.ReadLine();
                Console.Write("Enter name: ");
                _name = Console.ReadLine();
                Console.Write("Enter position: ");
                _position = Console.ReadLine();
                Console.Write("Enter phone number: ");
                _phonenumber = Console.ReadLine();
                Console.Write("Enter email: ");
                _email = Console.ReadLine();
                using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
                {
                    sw.WriteLine(_perscode + "|" + _surname + "|" + _name + "|" + _position + "|" + _phonenumber + "|" + _email);
                    sw.Close();
                }
            }

            
        }
        /// <summary>
        /// Виводить інформацію про всіх працівників.
        /// </summary>
        public override void PrintAll()
        {
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        PrintDataEmployee();
                    }
                    sr.Close();
                }
            }
            catch
            {
                Console.WriteLine("Error opening file!(Employee.cs PrintAll())");
            }
        }
        /// <summary>
        /// Виводить інформацію про одного працівника.
        /// </summary>
        private void PrintDataEmployee()
        {
            Console.WriteLine("Employee ID: " + _perscode);
            Console.WriteLine("Surname: " + _surname);
            Console.WriteLine("Name: " + _name);
            Console.WriteLine("Position: " + _position);
            Console.WriteLine("Phone number: " + _phonenumber);
            Console.WriteLine("Email: " + _email);
            Console.WriteLine("\n");
        }
        /// <summary>
        /// Шукає працівника серед файлу.
        /// </summary>
        /// <param name="searchedcode"></param>
        /// /// <returns>True or False</returns>
        public override bool SearchPersonOperation(int searchedcode)
        {
            bool search = false;
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        if (searchedcode == _perscode)
                        {
                            Console.WriteLine("Success! Record found. ");
                            search = true;
                            PrintDataEmployee();
                            break;
                        }
                        else
                        {
                            search = false;
                        }
                    }
                    sr.Close();

                }
            }
            catch
            {
                Console.WriteLine("Error opening file! (Employee.cs SearchPersonOperation()) ");
            }
            if (!search)
            {
                Console.WriteLine("Failed");
                return false;
            }
            return true;
        }
        public void Search()
        {
            Console.Write(" Enter code: ");
            int searchedcode = Convert.ToInt32(Console.ReadLine());
            SearchPersonOperation(searchedcode);
        }
        /// <summary>
        /// Видаляє запис про працівника.
        /// </summary>
        public void Delete()
        {
            DeletePerson(path, pathtemp);
        }
        /// <summary>
        /// Змінює інформацію про працівника.
        /// </summary>
        public override void EditPerson()
        {
            Console.Write("Enter the code to change: ");
            int id = Convert.ToInt32(Console.ReadLine());
            if (SearchPersonOperation(id))
            {                              
                    string line = null;                    
                    try
                    {
                        List<string> words = new List<string>();
                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                        {
                            using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                            {
                                while ((line = sr.ReadLine()) != null)
                                {
                                    words = (line.Split('|')).ToList();
                                    Divide(words);
                                    if (id == perscode)
                                    {
                                        Console.WriteLine("What to edit ? \n 1-Surname \n 2-Name \n 3-Position \n 4-Phone number \n 5-Email \n 0-Cancel operation.\n");
                                        int edit_chose = Convert.ToInt32(Console.ReadLine());
                                        switch (edit_chose)
                                        {
                                            case 1:
                                                {
                                                    Console.Write("New surname: ");
                                                    _surname = Console.ReadLine();
                                                }
                                                break;
                                            case 2:
                                                {
                                                    Console.Write("New name: ");
                                                    _name = Console.ReadLine();
                                                }
                                                break;
                                            case 3:
                                                {
                                                    Console.Write("New position: ");
                                                    _position = Console.ReadLine();
                                                }
                                                break;
                                            case 4:
                                                {
                                                    Console.Write("New phone number: ");
                                                    _phonenumber = Console.ReadLine();
                                                }
                                                break;
                                            case 5:
                                                {
                                                    Console.Write("New email: ");
                                                    _email = Console.ReadLine();
                                                }
                                                break;
                                            case 0:
                                                {
                                                    Console.Write("Operation canceled. ");
                                                }
                                                break;
                                            default:
                                                {
                                                    Console.WriteLine("You entered incorrect number.");
                                                }
                                                break;
                                        }
                                        Console.WriteLine("\n");
                                        PrintDataEmployee();
                                        sw.WriteLine(_perscode + "|" + _surname + "|" + _name + "|" + _position + "|" + _phonenumber + "|" + _email);
                                    }
                                    else
                                        if (id != _perscode)
                                        {
                                            sw.WriteLine(line);
                                        }
                                }
                                sw.Close();
                            }
                            sr.Close();
                        }
                        File.Delete(path);
                        File.Move(pathtemp, path);
                    }
                    catch
                    {
                        Console.WriteLine("Error opening file! (Employee.cs EditPerson())");
                    }
                
             }
             else
             {
                Console.WriteLine("Record does not exist.");
             }
        }
        /// <summary>
        /// Ділить запис в нашому файлі на слова.
        /// </summary>
        /// <param name="words"></param>
        private void Divide(List<string> words)
        {
            _perscode = Convert.ToInt32(words[0]);
            _surname = words[1];
            _name = words[2];
            _position = words[3];
            _phonenumber = words[4];
            _email = words[5];
        }
    }
}
