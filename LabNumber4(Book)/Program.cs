﻿using System;

namespace LabNumber4_Book_
{
    class Program
    {
        delegate void Work();
        static void WorkBook()
        {
            Book book = new Book();            
            do
            {
                Console.WriteLine("-----------------");
                Console.WriteLine("Select operation:\n 1-Add new book\n 2-Search book\n 3-Delete book\n 4-Edit record\n 5-Show full list \n Another number to go back higher.");
                Console.WriteLine("-----------------");
                int selectbook = Convert.ToInt32(Console.ReadLine());
                switch (selectbook)
                {
                    case 1:
                        {
                            book.AddBook();
                        }break;
                    case 2:
                        {
                            book.SearchBook();
                        }break;
                    case 3:
                        {
                            book.DeleteBook();
                        }break;
                    case 4:
                        {
                            book.EditBook();
                        }break;
                    case 5:
                        {
                            book.PrintFile();
                        }break;
                    default:
                        {
                            return;
                        }
                }
            } while (true);
        }

       static void WorkEmployee()
        {
            Employee employee = new Employee();
            do
            {
                Console.WriteLine("-----------------");
                Console.WriteLine("Select operation:\n 1-Add new employee\n 2-Search employee\n 3-Delete employee\n 4-Edit record\n 5-Show full list \n Another number to go back higher.");
                Console.WriteLine("-----------------");
                int selectemployee = Convert.ToInt32(Console.ReadLine());
                switch (selectemployee)
                {
                    case 1:
                        {
                            employee.AddNewPerson();
                        }
                        break;
                    case 2:
                        {
                            employee.Search();
                        }
                        break;
                    case 3:
                        {
                            employee.Delete();
                        }
                        break;
                    case 4:
                        {
                            employee.EditPerson();
                        }
                        break;
                    case 5:
                        {
                            employee.PrintAll();
                        }
                        break;
                    default:
                        {
                            return;
                        }
                }
            } while (true);
        }

        static void WorkCustomer()
        {
            Customer customer = new Customer();
            do
            {
                Console.WriteLine("-----------------");
                Console.WriteLine("Select operation:\n 1-Add new customer\n 2-Search customer\n 3-Delete customer\n 4-Edit record\n 5-Show full list \n Another number to go back higher.");
                Console.WriteLine("-----------------");
                int selectbook = Convert.ToInt32(Console.ReadLine());
                switch (selectbook)
                {
                    case 1:
                        {
                            customer.AddNewPerson();
                        }
                        break;
                    case 2:
                        {
                            customer.Search();
                        }
                        break;
                    case 3:
                        {
                            customer.Delete();
                        }
                        break;
                    case 4:
                        {
                            customer.EditPerson();
                        }
                        break;
                    case 5:
                        {
                            customer.PrintAll();
                        }
                        break;
                    default:
                        {
                            return;
                        }
                }
            } while (true);
        }
        static void Check()
        {
            Check check = new Check();
            check.CheckOperation();
        }
        static void Main(string[] args)
            {
            Work work;
                Console.WriteLine("---WELCOME---");
                do
                {

                    Console.WriteLine("Select: \n 1-Work with books \n 2-Work with employee\n 3-Work with customers\n 4-Make an order \n Another number to complete work.\n");
                    Console.WriteLine("Enter: ");
                    int selectmain = Convert.ToInt32(Console.ReadLine());
                    if(selectmain == 1)
                    {
                        work = WorkBook;
                    }
                    else if(selectmain == 2)
                    {
                        work = WorkEmployee;
                    }
                    else if(selectmain == 3)
                    {
                        work = WorkCustomer;
                    }
                    else if(selectmain == 4)
                    {
                        work = Check;
                    }
                    else
                    {
                        Console.WriteLine("---Bye---");
                        return;
                    }
                    work();
            } while (true);
        }
    }
}

