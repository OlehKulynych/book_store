﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

interface IBook
{
    void AddBook();
    void PrintDataBook();
    void DeleteBook();
    bool SearchedBookOperation(int searchedcode);
    void EditBook();
}

namespace LabNumber4_Book_
{
    class Book : IBook
    {
        private int _bookcode;
        private string _title;
        private string _author;
        private string _genre;
        private int _publicationyear;
        private int _numberpages;
        private string _language;
        private int _quantity;
        private double _price;
        private string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Book.txt");
        private string pathtemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "temp.txt");

        public int quantity
        {
            set 
            { 
                if (value <= _quantity && value > 0)
                { 
                    _quantity = value; 
                }
                else if(value == 0)
                {
                    Console.WriteLine("This product is not in stock.");
                }
                else 
                { 
                    Console.WriteLine("Not enough, we added the maximum."); 
                }
            }
            get { return _quantity; }
        }
        /// <summary>
        /// Додаємо нову книгу і записуємо дані в файл.
        /// </summary>
        public void AddBook()
        {          
            Console.Write("Enter the book code: ");
            _bookcode = Convert.ToInt32(Console.ReadLine());
            if (SearchedBookOperation(_bookcode))
            {
                Console.WriteLine("Such an entry exists, press 1 change the number?");
                int book = Convert.ToInt32(Console.ReadLine());
                if (book == 1)
                {
                    Console.Write("Enter how many to add: ");
                    int quantity = Convert.ToInt32(Console.ReadLine());
                    EditNumberCopies(-quantity, _bookcode);
                    Console.WriteLine("Quantity changed.");
                }
                else
                {
                    Console.WriteLine("Return...");
                }
            }
            else
            {
                Console.Write("Re-enter the book code: ");
                _bookcode = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter a title for the book: ");
                _title = Console.ReadLine();
                Console.Write("Enter the author: ");
                _author = Console.ReadLine();
                Console.Write("Enter genre: ");
                _genre = Console.ReadLine();
                Console.Write("Enter the year of publication: ");
                _publicationyear = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter the number of pages: ");
                _numberpages = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter the language: ");
                _language = Console.ReadLine();
                Console.Write("Enter the quantity: ");
                _quantity = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter the price: ");
                _price = Convert.ToDouble(Console.ReadLine());
                using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
                {
                    sw.WriteLine(_bookcode + "|" + _title + "|" + _author + "|" + _genre + "|" + _publicationyear + "|" + _numberpages + "|" + _language + "|" + _quantity + "|" + _price);
                    sw.Close();
                }
            }
                       
        }
        /// <summary>
        /// Виводить інформацію про всі книги.
        /// </summary>
        public void PrintFile()
        {                       
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        PrintDataBook();
                    }
                    sr.Close();
                }                        
            }
            catch
            {
                Console.WriteLine("Error opening file!(Book.cs PrintFile())");
            }
        }
        /// <summary>
        /// Виводить інформацію про одну книгу
        /// </summary>
        public void PrintDataBook()
        {        
            Console.WriteLine("Book code: " + _bookcode);
            Console.WriteLine("Title: " + _title);
            Console.WriteLine("Author: " + _author);
            Console.WriteLine("Genre: " + _genre);
            Console.WriteLine("Year of publication: " + _publicationyear);
            Console.WriteLine("Number of pages: " + _numberpages);
            Console.WriteLine("Language: " + _language);
            Console.WriteLine("Quantity: " + _quantity);
            Console.WriteLine("Price: " + _price);
            Console.WriteLine("\n");
        }
        
        public void SearchBook()
        {
            Console.Write(" Enter code: ");
            int searchedcode = Convert.ToInt32(Console.ReadLine());
            SearchedBookOperation(searchedcode);
        }
        /// <summary>
        /// Шукає книгу серед файлу.
        /// </summary>
        /// <param name="searchedcode"></param>
        /// /// <returns>True or False</returns>
        public bool SearchedBookOperation(int searchedcode)
        {
            bool search = false;
            try
            {
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    string line;
                    List<string> words = new List<string>();

                    while ((line = sr.ReadLine()) != null)
                    {
                        words = (line.Split('|')).ToList();
                        Divide(words);
                        if (searchedcode == _bookcode)
                        {
                            Console.WriteLine("Success! Record found. ");
                            search = true;
                            PrintDataBook();
                            break;
                        }
                        else
                        {
                            search = false;
                        }
                    }
                    sr.Close();
                  
                }
            }
            catch
            {
                Console.WriteLine("Error opening file! (Book.cs SearchedBookOperation()) ");
            }
            if (!search)
            {
                Console.WriteLine("Failed");
                return false;
            }
            return true;
        }
        /// <summary>
        /// Видаляє запис про книгу.
        /// </summary>
        public void DeleteBook()
        {
            Console.Write("Enter the code: ");
            int id = Convert.ToInt32(Console.ReadLine());
            if (SearchedBookOperation(id))
            {
                string line_to_delete = "";
                Console.Write("Press 1 to confirm the deletion: ");
                int confirmdelete = Convert.ToInt32(Console.ReadLine());
                if(confirmdelete == 1)
                {
                    bool delete = false;
                    try
                    {
                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                        {
                            string line;
                            List<string> words = new List<string>();
                            while ((line = sr.ReadLine()) != null)
                            {

                                words = (line.Split('|')).ToList();
                                _bookcode = Convert.ToInt32(words[0]);
                                if (id == _bookcode)
                                {
                                    line_to_delete = line;
                                    delete = true;
                                    break;
                                }
                                else
                                {
                                    delete = false;                                  
                                }
                            }
                            sr.Close();
                        }
                        if(!delete)
                        {
                            Console.WriteLine("You entered incorrect code");
                        }
                    }
                    catch
                    {
                        Console.WriteLine("Error opening file!(Book.cs DeleteBook(1))");
                    }
                    string str = null;
                    try
                    {
                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                        {
                            using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                            {
                                while ((str = sr.ReadLine()) != null)
                                {
                                    if (String.Compare(str, line_to_delete) == 0)
                                    {
                                        Console.WriteLine("\nSuccessful deleting.");
                                        continue;
                                    }
                                    sw.WriteLine(str);
                                }
                                sw.Close();
                            }
                            sr.Close();                           
                        }
                        File.Delete(path);
                        File.Move(pathtemp, path);
                        
                    }
                    catch
                    {
                        Console.WriteLine("Error opening file!(Book.cs DeleteBook(2))");
                    }
                }
                else
                {
                    Console.WriteLine("The delete operation was canceled.");
                }               
            }
            else
            {
                Console.WriteLine("Record does not exist.");
            }                                
        }
        /// <summary>
        /// Змінює інформацію про книгу.
        /// </summary>
        public void EditBook()
        {
            Console.Write("Enter the code to change: ");
            int id = Convert.ToInt32(Console.ReadLine());
            if (SearchedBookOperation(id))
            {                                      
                    string line = null;
                    try
                    {
                        List<string> words = new List<string>();
                        using (StreamReader sr = new StreamReader(path, Encoding.Default))
                        {
                            using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                            {
                                while ((line = sr.ReadLine()) != null)
                                {
                                    words = (line.Split('|')).ToList();
                                    Divide(words);
                                    if (id == _bookcode)
                                    {                                       
                                        Console.WriteLine("What to edit ? \n 1-Title \n 2-Author \n 3-Genre \n 4-Year of publication \n 5-Number of pages \n 6-Language \n 7-Quantity \n 8-Price \n 0 - Cancel editing\n");
                                        int edit_chose = Convert.ToInt32(Console.ReadLine());
                                        switch (edit_chose)
                                        {
                                            case 1:
                                                {
                                                    Console.Write("New Title: ");
                                                    _title = Console.ReadLine();
                                                }
                                                break;
                                            case 2:
                                                {
                                                    Console.Write("New author: ");
                                                    _author = Console.ReadLine();
                                                }
                                                break;
                                            case 3:
                                                {
                                                    Console.Write("New genre: ");
                                                    _genre = Console.ReadLine();
                                                }
                                                break;
                                            case 4:
                                                {
                                                    Console.Write("New year of publication: ");
                                                    _publicationyear = Convert.ToInt32(Console.ReadLine());
                                                }
                                                break;
                                            case 5:
                                                {
                                                    Console.Write("New number of pages: ");
                                                    _numberpages = Convert.ToInt32(Console.ReadLine());
                                                }
                                                break;
                                            case 6:
                                                {
                                                    Console.Write("New language: ");
                                                    _language = Console.ReadLine();
                                                }
                                                break;
                                            case 7:
                                                {
                                                    Console.Write("New quantity: ");
                                                    _quantity = Convert.ToInt32(Console.ReadLine());
                                                }
                                                break;
                                            case 8:
                                                {
                                                    Console.Write("New price: ");
                                                    _price = Convert.ToDouble(Console.ReadLine());
                                                }break;
                                            case 0:
                                                {
                                                    Console.Write("Operation canceled. ");
                                                }break;
                                            default:
                                                {
                                                    Console.WriteLine("You entered incorrect number.");
                                                }break;
                                        }
                                        Console.WriteLine("\n");
                                        PrintDataBook();
                                        sw.WriteLine(_bookcode + "|" + _title + "|" + _author + "|" + _genre + "|" + _publicationyear + "|" + _numberpages + "|" + _language + "|" + _quantity + "|" + _price);                                                                                
                                    }
                                    else
                                        if (id != _bookcode)
                                        {
                                            sw.WriteLine(line);
                                            
                                        }
                                }                                
                                sw.Close();
                            }
                            sr.Close();
                        }
                        File.Delete(path);
                        File.Move(pathtemp, path);                       
                    }
                    catch
                    {
                        Console.WriteLine("Error opening file! (Book.cs EditBook())");
                    }
                                
            }
            else
            {
                Console.WriteLine("Record does not exist.");
            }           
        }
        /// <summary>
        /// Змінює кількість книг.
        /// </summary>
        public void EditNumberCopies(int number, int id)
        {
            string line = null;
            try
            {
                List<string> words = new List<string>();
                using (StreamReader sr = new StreamReader(path, Encoding.Default))
                {
                    using (StreamWriter sw = new StreamWriter(pathtemp, true, Encoding.Default))
                    {
                        while ((line = sr.ReadLine()) != null)
                        {

                            words = (line.Split('|')).ToList();
                            Divide(words);
                            if (id == _bookcode)
                            {
                                _quantity -= number;
                                sw.WriteLine(_bookcode + "|" + _title + "|" + _author + "|" + _genre + "|" + _publicationyear + "|" + _numberpages + "|" + _language + "|" + _quantity + "|" + _price);
                            }
                            else
                                if (id != _bookcode)
                            {
                                sw.WriteLine(line);

                            }
                        }
                        sw.Close();
                    }
                    sr.Close();
                }
                File.Delete(path);
                File.Move(pathtemp, path);
            }
            catch
            {
                Console.WriteLine("Error opening file!(Book.cs EditNumberCopies())");
            }
        }
        /// <summary>
        /// Ділить запис в нашому файлі на слова.
        /// </summary>
        /// <param name="words"></param>
        private void Divide(List<string> words)
        {
            _bookcode = Convert.ToInt32(words[0]);
            _title = words[1];
            _author = words[2];
            _genre = words[3];
            _publicationyear = Convert.ToInt32(words[4]);
            _numberpages = Convert.ToInt32(words[5]);
            _language = words[6];
            _quantity = Convert.ToInt32(words[7]);
            _price = Convert.ToDouble(words[8]);
        }
    }  
}
